SRC_URL = https://gitlab.com/commonground/nlx/nlx/-/archive/v$(VERSION)/nlx-v$(VERSION).tar.gz
VERSION ?= 0.97.1
GO_VERSION = $(shell awk '/^go ([0-9.]+)$$/ { print $$2 }' src/nlx-v$(VERSION)/go.mod)
NODE_IMAGE = $(shell awk '/^FROM node:/ { print $$2 }' src/nlx-v$(VERSION)/management-ui/Dockerfile)
CERT_PORTAL_URL = https://certportal.demo.nlx.io

.PHONY: all
all: deb rpm

.PHONY: deb rpm
deb rpm: sources
	$(MAKE) -C $@ VERSION=$(VERSION) GO_VERSION=$(GO_VERSION) $@

.PHONY: sources
sources: src/nlx-v$(VERSION).tar.gz src/nlx-vendor-v$(VERSION).tar.gz src/nlx-management-ui-build-v$(VERSION).tar.gz

src/nlx-v$(VERSION).tar.gz:
	mkdir -p $(@D)
	curl --output $(@) $(SRC_URL)

src/nlx-v$(VERSION): src/nlx-v$(VERSION).tar.gz
	tar -zxf src/nlx-v$(VERSION).tar.gz -C $(@D)

src/nlx-vendor-v$(VERSION).tar.gz: src/nlx-v$(VERSION)/vendor
	tar -zcf $(@) -C src --exclude .git nlx-v$(VERSION)/vendor

src/nlx-v$(VERSION)/vendor: src/nlx-v$(VERSION)
	docker run --rm -v $(CURDIR)/src/nlx-v$(VERSION):/src/nlx-v$(VERSION) -w /src/nlx-v$(VERSION) golang:$(GO_VERSION)-alpine go mod vendor

src/nlx-management-ui-build-v$(VERSION).tar.gz: src/nlx-v$(VERSION)/management-ui/build
	tar -zcf $(@) -C src --exclude .git nlx-v$(VERSION)/management-ui/build

src/nlx-v$(VERSION)/management-ui/build: src/nlx-v$(VERSION)/management-ui/node_modules
	docker run --rm -v $(CURDIR)/src/nlx-v$(VERSION)/management-ui:/src/nlx-v$(VERSION)/management-ui -w /src/nlx-v$(VERSION)/management-ui $(NODE_IMAGE) npm run build

src/nlx-v$(VERSION)/management-ui/node_modules: src/nlx-v$(VERSION)
	docker run --rm -v $(CURDIR)/src/nlx-v$(VERSION)/management-ui:/src/nlx-v$(VERSION)/management-ui -w /src/nlx-v$(VERSION)/management-ui $(NODE_IMAGE) npm ci --no-progress --no-audit --color=false

.PHONY: certificate
certificate: tmp/nlx-root.crt tmp/org.crt

tmp/nlx-root.crt:
	mkdir -p $(@D)
	curl --output $(@) "$(CERT_PORTAL_URL)/root.crt"

tmp/org.crt: tmp/org.csr
	curl \
		--header "Content-Type: application/json" \
		--data "{\"csr\":\"$$(awk '{printf "%s\\n", $$0}' $<)\"}" \
		"$(CERT_PORTAL_URL)/api/request_certificate" \
	| python3 -c "import sys, json; print(json.load(sys.stdin)['certificate'])" \
	> $(@)

tmp/org.csr:
	mkdir -p $(@D)
	openssl req \
		-sha256 -nodes -newkey rsa:4096 \
		-keyout "$(@D)/org.key" -out "$(@D)/org.csr" \
		-subj "/C=NL/O=Packaging Test/CN=packaging-test.local"

.PHONY: clean
clean:
	$(RM) -rf rpm/debbuild/{build,sources}
	$(RM) -rf rpm/rpmbuild/{RPMS,SOURCES,SRPMS}
	$(RM) -r src/*
