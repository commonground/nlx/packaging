## Deprecation note: we don't release new packages anymore.

> Please [contact us](https://nlx.io/contact) when you have questions about this.
> 
> Read more about the supported deployment strategies for NLX:
> https://docs.nlx.io/nlx-in-production/introduction#deployment-strategies

# NLX packaging

Tools for packaging NLX.


## Building

```sh
make deb
make rpm
```

## Testing

Create testing certificate:

```sh
make certificate
```


Use Vagrant to start VMs to test the packges

```sh
vagrant up debian-buster
```
