#!/usr/bin/env bash
set -ex

export VERSION="${1}"
export SUITE="$(echo ${BUILD_IMAGE} | cut --delimiter=: --field 2)"
export CHANGELOG_DATE="$(date --rfc-2822)"

cd "${HOME}/debbuild"

for SOURCE in sources/*.tar.gz; do
  tar -xzf "${SOURCE}"
done

mv "nlx-v${VERSION}" "nlx-${VERSION}"
rm -rf "nlx-${VERSION}/debian"
cp -a debian "nlx-${VERSION}"

pushd "nlx-${VERSION}"

envsubst < ../debian/changelog > debian/changelog

dpkg-buildpackage -us -uc

popd
mv -v *.deb *.dsc build/
