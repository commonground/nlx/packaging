%global debug_package %{nil}

%global home_dir %{_sysconfdir}/%{name}


Name:             nlx
Version:          %{_version}
Release:          1%{?dist}
Summary:          NLX

License:          EUPL 1.2
URL:              https://nlx.io/
Source0:          https://gitlab.com/commonground/nlx/nlx/-/archive/v%{version}/%{name}-v%{version}.tar.gz
Source1:          %{name}-vendor-v%{version}.tar.gz
Source2:          %{name}-management-ui-build-v%{version}.tar.gz
Source3:          %{name}-inway.conf
Source4:          %{name}-inway.service
Source5:          %{name}-outway.conf
Source6:          %{name}-outway.service
Source7:          %{name}-management-api.conf
Source8:          %{name}-management-api.service
Source9:          %{name}-management-ui.conf
Source10:         %{name}-management-ui.service

BuildRequires:    systemd-units


%description
NLX


%prep
%autosetup -n %{name}-v%{version}
%autosetup -n %{name}-v%{version} -T -D -b 1
%autosetup -n %{name}-v%{version} -T -D -b 2


%build
for cmd in inway outway management-api management-ui; do
  /usr/local/go/bin/go build \
    -ldflags='-X "go.nlx.io/nlx/common/version.BuildVersion=%{version}"' \
    -o bin/nlx-$cmd \
    ./$cmd/cmd/nlx-$cmd
done


#%check
#for cmd in bin/*; do
#  $cmd --help
#done


%install
%{__install} -d -m 0755 %{buildroot}%{_bindir}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/%{name}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/%{name}/tls
%{__install} -d -m 0755 %{buildroot}%{_unitdir}

%{__install} -m 0755 bin/* %{buildroot}%{_bindir}/

# Inway
%{__install} -m 0640 %{SOURCE3} %{buildroot}%{_sysconfdir}/%{name}/%{name}-inway.conf
%{__install} -m 0644 %{SOURCE4} %{buildroot}%{_unitdir}/%{name}-inway.service

# Outway
%{__install} -m 0640 %{SOURCE5} %{buildroot}%{_sysconfdir}/%{name}/%{name}-outway.conf
%{__install} -m 0644 %{SOURCE6} %{buildroot}%{_unitdir}/%{name}-outway.service

# Management API
%{__install} -m 0640 %{SOURCE7} %{buildroot}%{_sysconfdir}/%{name}/%{name}-management-api.conf
%{__install} -m 0644 %{SOURCE8} %{buildroot}%{_unitdir}/%{name}-management-api.service

# Management UI
%{__install} -m 0640 %{SOURCE9} %{buildroot}%{_sysconfdir}/%{name}/%{name}-management-ui.conf
%{__install} -m 0644 %{SOURCE10} %{buildroot}%{_unitdir}/%{name}-management-ui.service
%{__install} -d -m 0755 %{buildroot}%{_usr}/share/%{name}-management-ui/public
%{__cp} -a management-ui/build/* %{buildroot}%{_usr}/share/%{name}-management-ui/public


%package inway
Summary:          NLX Inway
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description inway
NLX Inway

%pre inway
getent group %{name} &> /dev/null || \
  groupadd -r %{name} &> /dev/null
getent passwd %{name} &> /dev/null || \
  useradd -r -g %{name} -d %{home_dir} -s /sbin/nologin \
  -c 'NLX' %{name} &> /dev/null
exit 0

%preun inway
%systemd_preun %{name}-inway.service

%post inway
%systemd_post %{name}-inway.service

%postun inway
%systemd_postun_with_restart %{name}-inway.service

%files inway
%license LICENCE.md
%dir %{_sysconfdir}/%{name}/tls
%attr(0640, -, %{name}) %config(noreplace) %{_sysconfdir}/%{name}/%{name}-inway.conf
%attr(0755, -, -) %{_bindir}/%{name}-inway
%{_unitdir}/%{name}-inway.service



%package outway
Summary:          NLX Outway
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description outway
NLX Outway

%pre outway
getent group %{name} &> /dev/null || \
  groupadd -r %{name} &> /dev/null
getent passwd %{name} &> /dev/null || \
  useradd -r -g %{name} -d %{home_dir} -s /sbin/nologin \
  -c 'NLX' %{name} &> /dev/null
exit 0

%preun outway
%systemd_preun %{name}-outway.service

%post outway
%systemd_post %{name}-outway.service

%postun outway
%systemd_postun_with_restart %{name}-outway.service

%files outway
%license LICENCE.md
%dir %{_sysconfdir}/%{name}/tls
%attr(0640, -, %{name}) %config(noreplace) %{_sysconfdir}/%{name}/%{name}-outway.conf
%attr(0755, -, -) %{_bindir}/%{name}-outway
%{_unitdir}/%{name}-outway.service


%package management
Summary:          NLX Management
Requires:         %{name}-management-api = %{version}
Requires:         %{name}-management-ui = %{version}

%description management
NLX Management

%files management



%package management-api
Summary:          NLX Management API
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description management-api
NLX Management API server

%pre management-api
getent group %{name} &> /dev/null ||
  groupadd -r %{name} &> /dev/null
getent passwd %{name} &> /dev/null || \
  useradd -r -g %{name} -d %{home_dir} -s /sbin/nologin \
  -c 'NLX' %{name} &> /dev/null
exit 0

%preun management-api
%systemd_preun %{name}-management-api.service

%post management-api
%systemd_post %{name}-management-api.service

%postun management-api
%systemd_postun_with_restart %{name}-management-api.service

%files management-api
%license LICENCE.md
%dir %{_sysconfdir}/%{name}/tls
%attr(0640, -, %{name}) %config(noreplace) %{_sysconfdir}/%{name}/%{name}-management-api.conf
%attr(0755, -, -) %{_bindir}/%{name}-management-api
%{_unitdir}/%{name}-management-api.service



%package management-ui
Summary:          NLX Management UI
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description management-ui
NLX Management UI

%pre management-ui
getent group %{name} &> /dev/null || \
  groupadd -r %{name} &> /dev/null
getent passwd %{name} &> /dev/null || \
  useradd -r -g %{name} -d %{home_dir} -s /sbin/nologin \
  -c 'NLX' %{name} &> /dev/null
exit 0

%preun management-ui
%systemd_preun %{name}-management-ui.service

%post management-ui
%systemd_post %{name}-management-ui.service

%postun management-ui
%systemd_postun_with_restart %{name}-management-ui.service

%files management-ui
%license LICENCE.md
%attr(0640, -, %{name}) %config(noreplace) %{_sysconfdir}/%{name}/%{name}-management-ui.conf
%attr(0755, -, -) %{_bindir}/%{name}-management-ui
%{_unitdir}/%{name}-management-ui.service
%{_usr}/share/%{name}-management-ui/public


%changelog
