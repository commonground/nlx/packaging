#!/usr/bin/env sh

set -ex

SSH_HOSTNAME="${1}"
SSH_KEY_FILE="${2}"

# Create file to signal we're done with uploading
touch .done

/usr/bin/sftp -b - -i "${SSH_KEY_FILE}" "deploy@${SSH_HOSTNAME}" << EOF
  put deb/debbuild/build/debian-buster/*.deb deb/debian/buster
  put rpm/rpmbuild/RPMS/x86_64/*.el7.*.rpm rpm/el7/x86_64
  put rpm/rpmbuild/RPMS/x86_64/*.el8.*.rpm rpm/el8/x86_64
  put .done
EOF

rm .done
