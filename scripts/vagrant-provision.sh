#!/usr/bin/env sh

set -ex

TLS_DIR="/etc/nlx/tls"

if [ ! -d "${TLS_DIR}" ]; then
  exit 0
fi

if [ ! -f "${TLS_DIR}/localhost.crt" ]; then
  # Generate CA
  sudo openssl req \
    -x509 -sha256 -nodes -newkey rsa:4096 \
    -reqexts v3_req -extensions v3_ca \
    -keyout "${TLS_DIR}/root.key" -out "${TLS_DIR}/root.crt" \
    -subj "/C=NL/CN=NLX internal CA"

  # Create certificate request
  sudo openssl req \
    -sha256 -nodes -newkey rsa:4096 \
    -keyout "${TLS_DIR}/localhost.key" -out "${TLS_DIR}/localhost.csr" \
    -subj "/C=NL/CN=localhost" \
    -addext "subjectAltName = DNS:localhost" \
    -addext "certificatePolicies = 1.2.3.4"

  # Sign certificate
  sudo openssl x509 \
    -req -days 30 -CAcreateserial -sha256 \
    -CAkey "${TLS_DIR}/root.key" -CA "${TLS_DIR}/root.crt" \
    -in "${TLS_DIR}/localhost.csr" -out "${TLS_DIR}/localhost.crt"
fi

for c in nlx-inway nlx-management; do
  sudo ln -sf "localhost.key" "${TLS_DIR}/${c}.key"
  sudo ln -sf "localhost.crt" "${TLS_DIR}/${c}.crt"
done

sudo chmod 640 "${TLS_DIR}/localhost.key" "${TLS_DIR}/localhost.crt"
sudo chown root:nlx "${TLS_DIR}/localhost.key" "${TLS_DIR}/localhost.crt"

# Link organisation certificate
sudo ln -sf "/vagrant/tmp/nlx-root.crt" "/vagrant/tmp/org.crt" "/vagrant/tmp/org.key" "${TLS_DIR}/"
